<?php

namespace mroswald\Cashier;

use mroswald\Cashier\Concerns\ManagesCustomer;
use mroswald\Cashier\Concerns\ManagesSubscriptions;

trait Billable
{
    use ManagesCustomer;
//    use ManagesInvoices;
//    use ManagesPaymentMethods;
    use ManagesSubscriptions;
//    use PerformsCharges;
}
