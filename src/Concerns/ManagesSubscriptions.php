<?php

namespace mroswald\Cashier\Concerns;

use mroswald\Cashier\Subscription;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait ManagesSubscriptions {
    /**
     * Get all of the subscriptions for the Stripe model.
     *
     * @return HasMany
     */
    public function subscriptions() {
        return $this->hasMany(Subscription::class, $this->getForeignKey())
            ->orderBy('created_at', 'desc');
    }

    /**
     * Get a subscription instance by name.
     *
     * @param string $name
     *
     * @return Subscription|null
     */
    public function subscription($name = 'default') {
        return $this->subscriptions->sortByDesc(function (Subscription $subscription) {
            return $subscription->created_at->getTimestamp();
        })
            ->first(function (Subscription $subscription) use ($name) {
                return $subscription->name === $name;
            });
    }
}